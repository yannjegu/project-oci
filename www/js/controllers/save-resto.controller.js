myApp.controller('SaveRestoController', ['$scope', function($scope) {
    $scope.save = function(restaurant) {
        if(restaurant == null) {
            alert("Le nom, l'adresse et le nombre de places doivent etre remplis");
        }else{
            if(restaurant.name == null || restaurant.nbPlaces == null || restaurant.adresse == null || restaurant.codePostal==null){
                return;
            }else{
                saveResto(restaurant);
                //console.log(restaurant); 
            }
        
        }


    }

    function saveResto(restaurant) {
    	//select nom et adresse
    	// si le restaurant existe -> update
    	//si non -> INSERT
    	var id;
         var name = restaurant.name;
        var description = restaurant.description;
        var adresse = restaurant.adresse;
        var codeP = restaurant.codePostal;
        var ville = restaurant.ville;
        var nbPlace = restaurant.nbPlaces;
        var carte = restaurant.carte;
        var timeTable = restaurant.timeTable;
    	
        

        db = window.openDatabase("Disporesto", "1.0", "Disporesto", 0);

        var query = 'SELECT * FROM restaurant WHERE name = ? ';
        db.transaction(function (tx) {
            tx.executeSql(query, [name], function(tx, rs){
                var nb = rs.rows.length;
                for (var i = 0; i < nb; i++) {
                    var u = rs.rows.item(i);
                    if(adresse == u.adresse && codeP == u.codePostal){
                        var id = u.id;
                        break;
                        
                    }
                }
                if(id != null){
                    console.log("le restaurant existe dans la base");

                }else {
                    console.log("INSERT");
                    db.transaction(function(tx) {
                        tx.executeSql(
                        'INSERT INTO restaurant (name, user, numberPlace, longDescription, carte, timetable, adresse, codePostal, ville, specialite) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                        [name, 1, nbPlace, description, carte, timeTable, adresse, codeP, ville,1],
                        function(tx) {
                           console.log('Populated database OK');

                        }, function(error) {

                           console.log('Transaction ERROR: ' + error.message);
                        });
                    });
                }

            }, function(tx, rs){
              console.log("error");
            });
        });
      

/*
        
    	if(id == null){
    		*/
    	/*else {
    		// update
    		db.transaction(function(tx) {
			    tx.executeSql('UPDATE DemoTable SET RESTAURANT.nbCouverts = restaurant.nbCouverts');
			  }, function(error) {
			    console.log('Transaction ERROR: ' + error.message);
			  }, function() {
			    console.log('Populated database OK');
			  });
    	}
            });*/
    }
}]);

