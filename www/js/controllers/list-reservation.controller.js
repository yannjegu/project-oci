myApp.controller('ListReservationController', ['$scope', function($scope) {
  var u = window.sessionStorage.getItem("mail");
  if(u != null){
    //TODO get the current restaurant id
    getListReservation(1);
  }

  function getListReservation(id) {
    var query = 'SELECT * FROM reservation WHERE restaurant = ?';
    db = window.openDatabase("Disporesto", "1.0", "Disporesto", 0);

    db.transaction(function (tx) {
      tx.executeSql(query, [id], function(tx, rs){
        for(var i=0; i<rs.rows.length; i++) {
          var reservation = rs.rows.item(i);

          var table = document.getElementById("list-reservation");

          var row = table.insertRow();

          var firstName = row.insertCell(0);
          var lastName = row.insertCell(1);
          var number = row.insertCell(2);
          var cell4 = row.insertCell(3);

          number.innerHTML = reservation.number;

          query = 'SELECT * FROM user WHERE rowid = ?';
          tx.executeSql(query, [reservation.user], function(tx, rs){
            var user_info = rs.rows.item(0);
            firstName.innerHTML = user_info.firstName;
            lastName.innerHTML = user_info.lastName;
          });
        }
      }, function(tx, rs){
        console.log("error");
      });
    });
  }
}]);
