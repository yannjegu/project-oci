myApp.controller('RestauraterProfilController', ['$scope', function($scope) {

  db = window.openDatabase("Disporesto", "1.0", "Disporesto", 0);

  var firstName = window.sessionStorage.getItem("firstName");
  var lastName = window.sessionStorage.getItem("lastName");
  var mail = window.sessionStorage.getItem("mail");
  var password = window.sessionStorage.getItem("password");
  var id = window.sessionStorage.getItem("id");

  $scope.firstName = firstName;
  $scope.lastName = lastName;
  $scope.mail = mail;
  $scope.password = password;
  $scope.id = id;

  $scope.update = function(firstName, lastName, mail, password){
    db.transaction(function(tx) {
      tx.executeSql('UPDATE user SET firstName = ?, lastName = ?, mail = ?, password = ? WHERE id=?', [firstName, lastName, mail, password, id],
    function(error) {
      console.log('update OK');
      window.sessionStorage.setItem("firstName", firstName);
      window.sessionStorage.setItem("lastName", lastName);
      window.sessionStorage.setItem("mail", mail);
      window.sessionStorage.setItem("password", password);

    }, function() {
      console.log('Transaction ERROR: ' + error.message);
    });
  });
}
}]);
