myApp.controller('RestoCardController', ['$scope', function($scope) {

  if(true){
    //TODO get the current restaurant id
    getRestaurant(1);
  }

  function getRestaurant(id) {
    var query = 'SELECT * FROM restaurant WHERE id = ?';
    db = window.openDatabase("Disporesto", "1.0", "Disporesto", 0);

    db.transaction(function (tx) {
      tx.executeSql(query, [id], function(tx, rs){
        var restaurant = rs.rows.item(0);

        var title = document.getElementById("restaurant-title");
        title.innerHTML = restaurant.name;

        var description = document.getElementById("restaurant-longDescription");
        description.innerHTML = restaurant.longDescription;

        var carte = document.getElementById("restaurant-carte");
        carte.innerHTML = restaurant.carte;

        var timetable = document.getElementById("restaurant-timetable");
        timetable.innerHTML = restaurant.timetable;
      }, function(tx, rs){
        console.log("error");
      });
    });
  }
}]);
