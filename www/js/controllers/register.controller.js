myApp.controller('RegisterController', ['$scope', function($scope) {
    $scope.register = function(user) {
        if(user == null) {
            return;
        }
        if(isPasswordConfirmed(user)) {
            console.log(user);
            insertUser(user);

            // Used to reset the register form in the template
            $scope.user = {};
        }
    }

    function isPasswordConfirmed(user) {
        if(user.password == user.confirmedPassword && user.password != null) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
    * insertUser
    * WARNING: This method is asynchronous
    * WARNING: Check the name of field for user
    */
    function insertUser(user){
      /*		Create and init user table		*/
      db.transaction(function(tx) {
      	var first_name = user.firstName;
      	var last_name = user.lastName;
      	var password = user.password;
      	var mail = user.email;
        var isRestaurateur = user.isRestaurateur;
      	tx.executeSql(
        	'INSERT INTO user (firstName, lastName, password, mail, isRestaurateur) VALUES (?, ?, ?, ?, ?)',
        	[first_name, last_name, password, mail, isRestaurateur],
        	function(tx){
            //TODO connexion
          },
        	function(tx){
            //TODO manage error
          }
        );
      });
    }
}]);
