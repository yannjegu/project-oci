
function createTableReservation(){
  /*		Create and init reservation table		*/
  db.transaction(function(tx) {
  	tx.executeSql("CREATE TABLE IF NOT EXISTS reservation (user INTEGER, restaurant INTEGER, number INTEGER, FOREIGN KEY (user) REFERENCES User (id), FOREIGN KEY (restaurant) REFERENCES Restaurant (id), PRIMARY KEY(user, restaurant))", [], function(tx){}, onErrorDatabase);

  	tx.executeSql(
    	'INSERT INTO reservation (user, restaurant, number) VALUES (?, ?, ?)',
    	[1, 1, 4],
    	function(tx){},
    	function(tx){}
    );
  });
}
