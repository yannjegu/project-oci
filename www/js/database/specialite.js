function createTableSpecialite(){
  /*		Create and init specialite table		*/
  db.transaction(function(tx) {
  	tx.executeSql("CREATE TABLE IF NOT EXISTS specialite (id INTEGER PRIMARY KEY, name TEXT)", [], function(tx){}, onErrorDatabase);

  	tx.executeSql(
    	'INSERT INTO specialite (id, name) VALUES (?, ?)',
    	[1, "Indien"],
      [2, "Italien"],
    	function(tx){},
    	function(tx){}
    );
  });
}
