var myApp = angular.module('myApp', ["ngRoute"]);
myApp.config(function($routeProvider, $locationProvider) {
            $locationProvider.hashPrefix('');
            $routeProvider
            .when('/', {
                templateUrl: './views/home.html',
                controller: 'HomeController'
            })
            .when('/resto-card', {
                templateUrl: './views/resto-card.html',
                controller: 'RestoCardController'
            })
            .when('/restaurater-profil', {
                templateUrl: './views/restaurater-profil.html', controller: 'RestauraterProfilController'
            })
            .when('/list-reservation', {
                templateUrl: './views/list-reservation.html', controller: 'ListReservationController'
            })
            .when('/editer-restaurant', {
                templateUrl: './views/form-restaurant.html', controller: 'SaveRestoController'
            });
        });
